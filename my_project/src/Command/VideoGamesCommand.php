<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VideoGamesCommand extends Command
{
    const PLAYER_1 = ['name' => 'player1', 'games' => ['BF1', 'WOW', 'LOL', 'Dota', 'Dirt Rallye']];
    const PLAYER_2 = ['name' => 'player2', 'games' => ['BF4', 'BF1', 'CS:GO']];
    const PLAYER_3 = ['name' => 'player3', 'games' => ['Dota', 'LOL', 'BF1']];

    const PLAYERS = [self::PLAYER_1, self::PLAYER_2, self::PLAYER_3];

    protected function configure()
    {
        $this
            ->addArgument('game', InputArgument::OPTIONAL, 'The game you search for in players\' list.')
            ->setName('app:games')
            ->setDescription('A super useful command to play with your friends!');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // TODO: display games in common for every player
        $output->writeln(array_intersect(self::PLAYER_1['games'], self::PLAYER_2['games'], self::PLAYER_3['games']));

        // TODO: type a game name as input and display players that have this game
        // TODO: if no player has it, display a message
        if ($input->hasArgument('game')) {
            $game = $input->getArgument('game');
            $found = false;
            foreach (self::PLAYERS as $player) {
                if (in_array($game, $player['games'])) {
                    $output->writeln('Player '.$player['name'].' has game '.$game);
                    $found = true;
                }
            }
            if (!$found) {
                $output->writeln('no player has this game');
            }
        }
    }
}
